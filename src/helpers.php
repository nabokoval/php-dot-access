<?php
/**
 * Dot - PHP dot notation access to arrays
 *
 * @author  Alexei Nabokov <aleiankov@shenkaimal.ch>
 * @link    https://bitbucket.com/nabokoval/php-dot-notation
 * @license https://bitbucket.com/nabokoval/php-dot-notation/blob/master/LICENSE.md (MIT License)
 */

use Nabokoval\Dot;

if (! function_exists('dot')) {
    /**
     * Create a new Dot object with the given items
     *
     * @param  mixed $items
     * @return Dot
     */
    function dot($items)
    {
        return new Dot($items);
    }
}
